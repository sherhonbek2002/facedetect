import cv2 as cv
import os
from os.path import join
import numpy as np
from random import choice

DIR = 'val/'

haar_cascade = cv.CascadeClassifier('haar_face.xml')
person_name = choice(os.listdir('val/'))
person_path = join(DIR,person_name)

img_name = choice(os.listdir(person_path))
img_path = join(person_path,img_name)

img = cv.imread(img_path)
gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)

rec = haar_cascade.detectMultiScale(gray,scaleFactor=1.2,minNeighbors=3)

for (x,y,w,h) in rec:
    cv.rectangle(img,(x, y), (x + w, y + h), (0, 255, 0), 2)

cv.imshow("Face",img)

cv.waitKey(0)